package AerialVehicles.PlaneKinds;

import AerialVehicles.AerialVehicle;
import AerialVehicles.ENUM.CameraKind;
import AerialVehicles.ENUM.FlightStatus;
import Entities.Coordinates;

public class BdaAerialVehicle extends AerialVehicle {
    private CameraKind cameraKind;

    public CameraKind getCameraKind() {
        return cameraKind;
    }

    public BdaAerialVehicle(double flightHoursFromLastFix, FlightStatus flightStatus, Coordinates baseCampCoordinates, int hoursTillFix, CameraKind cameraKind) {
        super(flightHoursFromLastFix, flightStatus, baseCampCoordinates, hoursTillFix);
        this.cameraKind = cameraKind;
    }
}
