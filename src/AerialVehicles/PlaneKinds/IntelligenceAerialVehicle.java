package AerialVehicles.PlaneKinds;

import AerialVehicles.AerialVehicle;
import AerialVehicles.ENUM.FlightStatus;
import AerialVehicles.ENUM.SensorKind;
import Entities.Coordinates;

public class IntelligenceAerialVehicle extends AerialVehicle {
    private SensorKind sensorKind;

    public IntelligenceAerialVehicle(double flightHoursFromLastFix, FlightStatus flightStatus, Coordinates baseCampCoordinates, int hoursTillFix, SensorKind sensorKind) {
        super(flightHoursFromLastFix, flightStatus, baseCampCoordinates, hoursTillFix);
        this.sensorKind = sensorKind;
    }

    public SensorKind getSensorKind() {
        return sensorKind;
    }
}
