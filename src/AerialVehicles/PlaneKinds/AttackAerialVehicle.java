package AerialVehicles.PlaneKinds;

import AerialVehicles.AerialVehicle;
import AerialVehicles.ENUM.FlightStatus;
import AerialVehicles.ENUM.RocketKind;
import Entities.Coordinates;

public class AttackAerialVehicle extends AerialVehicle {
    private int numberOfRockets;
    private RocketKind rocketKind;

    public AttackAerialVehicle(double flightHoursFromLastFix, FlightStatus flightStatus, Coordinates baseCampCoordinates, int hoursTillFix, int numberOfRockets, RocketKind rocketKind) {
        super(flightHoursFromLastFix, flightStatus, baseCampCoordinates, hoursTillFix);
        this.numberOfRockets = numberOfRockets;
        this.rocketKind = rocketKind;
    }

    public int getNumberOfRockets() {
        return numberOfRockets;
    }

    public RocketKind getRocketKind() {
        return rocketKind;
    }
}
