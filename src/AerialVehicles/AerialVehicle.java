package AerialVehicles;


import AerialVehicles.ENUM.FlightStatus;
import Entities.Coordinates;

public abstract class AerialVehicle {
    private double flightHoursFromLastFix;
    private FlightStatus flightStatus;
    private Coordinates baseCampCoordinates;
    private int hoursTillFix;

    public AerialVehicle(double flightHoursFromLastFix, FlightStatus flightStatus, Coordinates baseCampCoordinates,
                                                                                                    int hoursTillFix) {
        this.flightHoursFromLastFix = flightHoursFromLastFix;
        this.flightStatus = flightStatus;
        this.baseCampCoordinates = baseCampCoordinates;
        this.hoursTillFix = hoursTillFix;
    }

    public FlightStatus getFlightStatus() {
        return flightStatus;
    }

    public void setFlightStatus(FlightStatus flightStatus) {
        this.flightStatus = flightStatus;
    }

    public void flyTo(Coordinates destination) {
        if (flightStatus == FlightStatus.READY) {
            System.out.printf("Flying to %s, %s %n",destination.getLongitude(),destination.getLatitude());
            flightStatus = FlightStatus.IN_FLIGHT;
        }
        else{
            System.out.println("Aerial Vehicle isn't ready to fly %n");
        }
    }

    public Coordinates getBaseCampCoordinates() {
        return baseCampCoordinates;
    }

    public String getPlatformName() {
        return getClass().getSimpleName();
    }

    public int getHoursTillFix(){
        return hoursTillFix;
    }

    public void setHoursTillFix(int hoursTillFix) {
        this.hoursTillFix = hoursTillFix;
    }

    public void land(Coordinates destination) {
        System.out.printf("Landing On: %s, %s %n", destination.getLongitude(),destination.getLatitude());
        check();
    }

    public void check() {

        if (this.flightHoursFromLastFix >= this.hoursTillFix) {
            this.flightStatus = FlightStatus.NOT_READY;
            repair();
        } else {
            this.flightStatus = FlightStatus.READY;
        }

    }

    public void repair() {
        this.flightHoursFromLastFix = 0;
        this.flightStatus = FlightStatus.READY;
    }



}

