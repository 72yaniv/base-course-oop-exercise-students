package AerialVehicles.FighterAircraft;

import AerialVehicles.ENUM.FlightStatus;
import AerialVehicles.ENUM.RocketKind;
import AerialVehicles.PlaneKinds.AttackAerialVehicle;
import Entities.Coordinates;

public class FighterAircraft extends AttackAerialVehicle {
    private static final int hoursTillFix = 250;

    public FighterAircraft(double flightHoursFromLastFix, FlightStatus flightStatus, Coordinates baseCampCoordinates, int numberOfRockets, RocketKind rocketKind) {
        super(flightHoursFromLastFix, flightStatus, baseCampCoordinates, hoursTillFix, numberOfRockets, rocketKind);

    }
}
