package AerialVehicles.FighterAircraft;


import AerialVehicles.ENUM.CameraKind;
import AerialVehicles.ENUM.FlightStatus;
import AerialVehicles.ENUM.RocketKind;
import AerialVehicles.PlaneKinds.BdaAerialVehicle;
import Entities.Coordinates;

public class F16 extends FighterAircraft {
    private BdaAerialVehicle bdaPlane;

    public F16(double flightHoursFromLastFix, FlightStatus flightStatus, Coordinates baseCampCoordinates, CameraKind cameraKind, int numberOfRockets, RocketKind rocketKind) {
        super(flightHoursFromLastFix, flightStatus, baseCampCoordinates, numberOfRockets, rocketKind);
        this.bdaPlane = new BdaAerialVehicle(flightHoursFromLastFix, flightStatus, baseCampCoordinates, this.getHoursTillFix(), cameraKind);

    }

    public BdaAerialVehicle getBdaPlane() {
        return bdaPlane;
    }
}
