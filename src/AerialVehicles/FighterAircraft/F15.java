package AerialVehicles.FighterAircraft;


import AerialVehicles.ENUM.FlightStatus;
import AerialVehicles.ENUM.RocketKind;
import AerialVehicles.ENUM.SensorKind;
import AerialVehicles.PlaneKinds.IntelligenceAerialVehicle;
import Entities.Coordinates;

public class F15 extends FighterAircraft {
    private IntelligenceAerialVehicle intelligencePlane;

    public F15(double flightHoursFromLastFix, FlightStatus flightStatus, Coordinates baseCampCoordinates, SensorKind sensorKind, int numberOfRockets, RocketKind rocketKind) {
        super(flightHoursFromLastFix, flightStatus, baseCampCoordinates, numberOfRockets, rocketKind);
        this.intelligencePlane = new IntelligenceAerialVehicle(flightHoursFromLastFix, flightStatus, baseCampCoordinates, this.getHoursTillFix(), sensorKind);
    }

    public IntelligenceAerialVehicle getIntelligencePlane() {
        return intelligencePlane;
    }
}
