package AerialVehicles.ENUM;

public enum FlightStatus {
    READY,
    NOT_READY,
    IN_FLIGHT
}
