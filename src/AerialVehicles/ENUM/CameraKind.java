package AerialVehicles.ENUM;

public enum CameraKind {
    Regular,
    Thermal,
    NightVision
}
