package AerialVehicles.UAV.Haron;


import AerialVehicles.ENUM.CameraKind;
import AerialVehicles.ENUM.FlightStatus;
import AerialVehicles.ENUM.RocketKind;
import AerialVehicles.ENUM.SensorKind;
import AerialVehicles.PlaneKinds.AttackAerialVehicle;
import AerialVehicles.PlaneKinds.BdaAerialVehicle;
import Entities.Coordinates;

public class Shoval extends Haron {
    BdaAerialVehicle bdaPlane;
    AttackAerialVehicle attackPlane;

    public Shoval(double flightHoursFromLastFix, FlightStatus flightStatus, Coordinates baseCampCoordinates, SensorKind sensorKind, CameraKind cameraKind, int numberOfRockets, RocketKind rocketKind) {
        super(flightHoursFromLastFix, flightStatus, baseCampCoordinates, sensorKind);
        this.attackPlane = new AttackAerialVehicle(flightHoursFromLastFix, flightStatus, baseCampCoordinates, this.getHoursTillFix(), numberOfRockets, rocketKind);
        this.bdaPlane = new BdaAerialVehicle(flightHoursFromLastFix, flightStatus, baseCampCoordinates, this.getHoursTillFix(), cameraKind);

    }

    public BdaAerialVehicle getBdaPlane() {
        return bdaPlane;
    }

    public AttackAerialVehicle getAttackPlane() {
        return attackPlane;
    }
}

