package AerialVehicles.UAV.Haron;

import AerialVehicles.ENUM.FlightStatus;
import AerialVehicles.ENUM.SensorKind;
import AerialVehicles.UAV.UnmannedAerialVehicle;
import Entities.Coordinates;

public abstract class Haron extends UnmannedAerialVehicle {
    private static final int hoursTillFix = 150;

    public Haron(double flightHoursFromLastFix, FlightStatus flightStatus, Coordinates baseCampCoordinates, SensorKind sensorKind) {
        super(flightHoursFromLastFix, flightStatus, baseCampCoordinates, hoursTillFix, sensorKind);
    }
}

