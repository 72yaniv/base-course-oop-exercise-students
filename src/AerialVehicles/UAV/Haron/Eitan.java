package AerialVehicles.UAV.Haron;

import AerialVehicles.ENUM.FlightStatus;
import AerialVehicles.ENUM.RocketKind;
import AerialVehicles.ENUM.SensorKind;
import AerialVehicles.PlaneKinds.AttackAerialVehicle;
import Entities.Coordinates;

public class Eitan extends Haron {
    private AttackAerialVehicle attackPlane;

    public Eitan(double flightHoursFromLastFix, FlightStatus flightStatus, Coordinates baseCampCoordinates, SensorKind sensorKind, int numberOfRockets, RocketKind rocketKind) {
        super(flightHoursFromLastFix, flightStatus, baseCampCoordinates, sensorKind);
        this.attackPlane = new AttackAerialVehicle(flightHoursFromLastFix, flightStatus, baseCampCoordinates, this.getHoursTillFix(), numberOfRockets, rocketKind);

    }

    public AttackAerialVehicle getAttackPlane() {
        return attackPlane;
    }
}
