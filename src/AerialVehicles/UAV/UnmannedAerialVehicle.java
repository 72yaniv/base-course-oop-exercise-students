package AerialVehicles.UAV;

import AerialVehicles.ENUM.FlightStatus;
import AerialVehicles.ENUM.SensorKind;
import AerialVehicles.PlaneKinds.IntelligenceAerialVehicle;
import Entities.Coordinates;

public class UnmannedAerialVehicle extends IntelligenceAerialVehicle {
    public UnmannedAerialVehicle(double flightHoursFromLastFix, FlightStatus flightStatus,
                                            Coordinates baseCampCoordinates, int hoursTillFix, SensorKind sensorKind) {
        super(flightHoursFromLastFix, flightStatus, baseCampCoordinates, hoursTillFix, sensorKind);
    }

    public void hoverOverLocation(Coordinates destination){
        this.setFlightStatus(FlightStatus.IN_FLIGHT);
        System.out.printf("Hovering Over: %s, %s%n",destination.getLongitude(),destination.getLatitude());
    }

}