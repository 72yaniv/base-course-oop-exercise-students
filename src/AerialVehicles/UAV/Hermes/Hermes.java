package AerialVehicles.UAV.Hermes;

import AerialVehicles.ENUM.FlightStatus;
import AerialVehicles.ENUM.SensorKind;
import AerialVehicles.UAV.UnmannedAerialVehicle;
import Entities.Coordinates;

public abstract class Hermes extends UnmannedAerialVehicle {

    private static final int hoursTillFix = 100;

    public Hermes(double flightHoursFromLastFix, FlightStatus flightStatus, Coordinates baseCampCoordinates, SensorKind sensorKind) {
        super(flightHoursFromLastFix, flightStatus, baseCampCoordinates, hoursTillFix, sensorKind);
    }

}

