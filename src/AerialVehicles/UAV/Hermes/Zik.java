package AerialVehicles.UAV.Hermes;


import AerialVehicles.ENUM.CameraKind;
import AerialVehicles.ENUM.FlightStatus;
import AerialVehicles.ENUM.SensorKind;
import AerialVehicles.PlaneKinds.BdaAerialVehicle;
import Entities.Coordinates;

public class Zik extends Hermes{
    BdaAerialVehicle bdaPlane;

    public Zik(double flightHoursFromLastFix, FlightStatus flightStatus, Coordinates baseCampCoordinates, SensorKind sensorKind, CameraKind cameraKind) {
        super(flightHoursFromLastFix, flightStatus, baseCampCoordinates, sensorKind);
        this.bdaPlane = new BdaAerialVehicle(flightHoursFromLastFix,flightStatus,baseCampCoordinates,this.getHoursTillFix(),cameraKind);
    }

    public BdaAerialVehicle getBdaPlane() {
        return bdaPlane;
    }

}