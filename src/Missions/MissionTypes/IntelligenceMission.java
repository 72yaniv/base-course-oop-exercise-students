package Missions.MissionTypes;

import AerialVehicles.AerialVehicle;
import AerialVehicles.PlaneKinds.IntelligenceAerialVehicle;
import Entities.Coordinates;
import Missions.Mission;

public class IntelligenceMission extends Mission {
    private String region;
    private IntelligenceAerialVehicle intelligencePlane;

    public IntelligenceMission(Coordinates target, String pilotName, String region, IntelligenceAerialVehicle intelligencePlane) {
        super(target, pilotName, intelligencePlane);
        this.region = region;
        this.intelligencePlane = intelligencePlane;
    }

    public String getRegion() {
        return region;
    }

    @Override
    public String executeMission() {
        return String.format("%s: %s Collecting data in %s with: %s", this.getPilotName(), intelligencePlane.getPlatformName(),
                this.region, intelligencePlane.getSensorKind());
    }

}
