package Missions.MissionTypes;

import AerialVehicles.PlaneKinds.AttackAerialVehicle;
import Entities.Coordinates;
import Missions.Mission;

public class AttackMission extends Mission {


    private String target;
    private AttackAerialVehicle attackPlane;

    public AttackMission(Coordinates target, String pilotName, String target1, AttackAerialVehicle attackPlane) {
        super(target, pilotName, attackPlane);
        this.target = target1;
        this.attackPlane = attackPlane;
    }

    @Override
    public String executeMission() {
        return String.format("%s: %s Attacking %s with: %sX%s", this.getPilotName(), attackPlane.getPlatformName(),
                                            this.target, attackPlane.getRocketKind(), attackPlane.getNumberOfRockets());
    }
}
