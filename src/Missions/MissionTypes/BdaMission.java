package Missions.MissionTypes;

import AerialVehicles.PlaneKinds.BdaAerialVehicle;
import Entities.Coordinates;
import Missions.Mission;

public class BdaMission extends Mission {
    private String objective;
    private BdaAerialVehicle bdaPlane;

    public BdaMission(Coordinates target, String pilotName, String objective, BdaAerialVehicle bdaPlane) {
        super(target, pilotName, bdaPlane);
        this.objective = objective;
        this.bdaPlane = bdaPlane;
    }

    @Override
    public String executeMission() {
        return String.format("%s: %s Collecting data in %s with: %s", this.getPilotName(), bdaPlane.getPlatformName(),
                this.objective, bdaPlane.getCameraKind());
    }

}
