package Missions;

import AerialVehicles.AerialVehicle;
import Entities.Coordinates;

public abstract class Mission {
    private Coordinates target;
    private String pilotName;
    private AerialVehicle av;

    public Mission(Coordinates target, String pilotName, AerialVehicle av) {
        this.target = target;
        this.pilotName = pilotName;
        this.av = av;
    }

    public String getPilotName() {
        return pilotName;
    }

    public void begin() {
        System.out.println("Beginning Mission!");
        av.flyTo(this.target);
    }

    public void cancel() {
        System.out.println("Abort Mission!");
        av.land(av.getBaseCampCoordinates());

    }

    public void finish() {
        this.executeMission();
        av.land(av.getBaseCampCoordinates());
        System.out.println("Finish Mission!\n");
    }

    public abstract String executeMission();

}
