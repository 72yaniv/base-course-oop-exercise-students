import AerialVehicles.ENUM.CameraKind;
import AerialVehicles.ENUM.FlightStatus;
import AerialVehicles.ENUM.RocketKind;
import AerialVehicles.ENUM.SensorKind;
import AerialVehicles.FighterAircraft.F15;
import AerialVehicles.FighterAircraft.F16;
import AerialVehicles.PlaneKinds.AttackAerialVehicle;
import AerialVehicles.PlaneKinds.BdaAerialVehicle;
import AerialVehicles.PlaneKinds.IntelligenceAerialVehicle;
import AerialVehicles.UAV.Haron.Eitan;
import AerialVehicles.UAV.Hermes.Zik;
import AerialVehicles.UAV.UnmannedAerialVehicle;
import Entities.Coordinates;
import Missions.MissionTypes.AttackMission;
import Missions.MissionTypes.BdaMission;
import Missions.MissionTypes.IntelligenceMission;

public class Main {

    public static void main(String[] args) {
	// Implement scenarios from readme here
        Coordinates baseCampCorr = new Coordinates(31.827604665263365, 34.81739714569337);

        UnmannedAerialVehicle intelligencePlane = new Eitan(0, FlightStatus.READY,baseCampCorr, SensorKind.InfraRed,4,RocketKind.Python);
        Coordinates imCorr = new Coordinates(33.2033427805222, 44.5176910795946);
        IntelligenceMission im = new IntelligenceMission(imCorr,"Dror zalicha","Iraq",intelligencePlane);
        im.begin();
        intelligencePlane.hoverOverLocation(imCorr);
        System.out.println(im.executeMission());
        im.finish();



        AttackAerialVehicle attackPlane = new F15(0, FlightStatus.READY,baseCampCorr, SensorKind.InfraRed,250,RocketKind.Spice250);
        Coordinates amCorr = new Coordinates(33.2033427805222, 44.5176910795946);
        AttackMission am = new AttackMission(amCorr,"Ze'ev Raz","Tuwaitha Nuclear Research Center",attackPlane);
        am.begin();
        System.out.println(am.executeMission());
        am.finish();



        F16 f16 = new F16(0, FlightStatus.READY,baseCampCorr, CameraKind.Thermal,250,RocketKind.Spice250);
        Coordinates bmCorr = new Coordinates(33.2033427805222, 44.5176910795946);
        BdaMission bm = new BdaMission(bmCorr,"Ilan Ramon","Tuwaitha Nuclear Research Center",f16.getBdaPlane());
        bm.begin();
        System.out.println(bm.executeMission());
        bm.finish();


        System.out.println("Good Job!! You Get an A Score ;)");
    }
}
